from typing import List

def binary_search(value: int, stack: List[int]) -> int:
    """Binary Search Algorithm"""

    left, right = 0, len(stack) - 1

    while (left <= right):
        mid = (left + right) // 2

        if stack[mid] == value:
            return mid

        if (value > stack[mid]):
            left = mid + 1
        else:
            right = mid - 1

    raise ValueError("value not in stack")


def linear_search(search: int, stack: List[int]) -> int:
    """Linear search algorithm"""
    for i,v in enumerate(stack):
        if v == search:
            return i

        if v > search:
            raise ValueError("value not in stack")
        
