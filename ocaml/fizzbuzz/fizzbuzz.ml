let fizz_buzz n =
	match n mod 3, n mod 5 with
	| 0, 0 -> "fizz buzz"
	| 0, _ -> "fizz"
	| _, 0 -> "buzz"
	| _, _ -> string_of_int n

let () =
	let number = [1;2;3;4;5;6;7;8;9;10;11;12;13;14;15] in
	let rec loop lst =
		match lst with
		| [] -> print_endline "empty list"
		| hd :: tl -> print_endline (fizz_buzz hd); loop tl
	in
	loop number

